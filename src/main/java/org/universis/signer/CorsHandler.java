package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;

import java.util.Map;

public class CorsHandler implements RouterNanoHTTPD.UriResponder {

    @Override
    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return this.other("PATCH", uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return this.other("PUT", uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return this.other("POST", uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return this.other("DELETE", uriResource, map, ihttpSession);
    }

    /**
     * Appends CORS headers to a response
     * @param res The target response
     */
    public static void enable(NanoHTTPD.Response res) {
        // add cors
        res.addHeader("Access-Control-Allow-Origin", "*");
        res.addHeader("Access-Control-Allow-Methods", "GET,OPTIONS,PUT,POST,PATCH,DELETE");
        res.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Authorization,Content-Type");
    }

    /**
     * Appends CORS headers to a response based on a specific request
     * @param req An incoming request
     * @param res The target response
     */
    public static void enable(NanoHTTPD.IHTTPSession req, NanoHTTPD.Response res) {
        // add cors
        res.addHeader("Access-Control-Allow-Origin", "*");
        res.addHeader("Access-Control-Allow-Methods", "GET,OPTIONS,PUT,POST,PATCH,DELETE");
        res.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Authorization,Content-Type");
        Map<String, String> headers = req.getHeaders();
        if (headers.containsKey("origin")) {
            res.addHeader("Access-Control-Allow-Origin", headers.get("origin"));
        }
    }

    @Override
    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        NanoHTTPD.Response res = NanoHTTPD.newFixedLengthResponse(NanoHTTPD.Response.Status.OK, null, null);
        CorsHandler.enable(res);
        return res;
    }
}
