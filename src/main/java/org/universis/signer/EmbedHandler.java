package org.universis.signer;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmbedHandler implements RouterNanoHTTPD.UriResponder {

    private static final Logger log = LogManager.getLogger(SignerHandler.class);
    public HashMap<String,String> files = null;

    @Override
    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        try {
            // parse body
            this.files = new HashMap<>();
            ihttpSession.parseBody(this.files);
            String file = null;
            for(String key : this.files.keySet()) {
                if (key.startsWith("file")) {
                    file = this.files.get(key);
                    break;
                }
            }
            if (file == null) {
                return new BadRequestHandler("Parameter file may not be null.").get(uriResource, map, ihttpSession);
            }
            // get form data
            Map<String, List<String>> params = ihttpSession.getParameters();
            // get file
            if (!params.containsKey("file")) {
                return new BadRequestHandler("File parameter is missing").get(uriResource, map, ihttpSession);
            }
            String inFile = params.get("file").get(0);
            String fileType = "." + FilenameUtils.getExtension(inFile).toLowerCase();
            switch (fileType) {
                case ".docx":
                case ".xlsx":
                    String outFileName = File.createTempFile("signatureLine", fileType).getAbsolutePath();
                    File outFile = new File(outFileName);
                    new OfficeDocumentSigner().embedSignatureLine(new File(file), outFile);
                    // set content type
                    String contentType;
                    if (fileType.equals(".xlsx")) {
                        contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    } else {
                        contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    }
                    // return output
                    String contentDisposition = "attachment; filename=" + outFile.getName()  + "\"";
                    NanoHTTPD.Response res = new FileStreamHandler(outFileName, contentType).get(uriResource, map, ihttpSession);
                    res.addHeader("Content-Disposition", contentDisposition);
                    CorsHandler.enable(res);
                    Map<String, String> headers = ihttpSession.getHeaders();
                    if (headers.containsKey("origin")) {
                        res.addHeader("Access-Control-Allow-Origin", headers.get("origin"));
                    }
                    return res;
                default:
                    return new BadRequestHandler("File type is not supported.").get(uriResource, map, ihttpSession);
            }

        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
        }


    }

    @Override
    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new CorsHandler().other(s, uriResource, map, ihttpSession);
    }
}
