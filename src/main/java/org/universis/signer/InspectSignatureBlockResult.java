package org.universis.signer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

@JsonSerialize
public class InspectSignatureBlockResult implements Serializable {
    @JsonProperty
    public int page;
    @JsonProperty
    public int[] position;

}
