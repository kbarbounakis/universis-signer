package org.universis.signer;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import dorkbox.systemTray.SystemTray;
import dorkbox.systemTray.MenuItem;
import org.apache.commons.cli.*;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class SignerController extends JDialog {
    private JPanel contentPane;
    private JLabel splash;
    private JButton buttonStartStop;
    private JButton buttonCancel;
    private JLabel lastMessage;
    private static SignerApp signerApp;
    private static SystemTray systemTray;
    private static SignerController currentController;
    private static final Logger log = LogManager.getLogger(SignerController.class);
    private boolean standalone = false;

    public SignerController() {
        this.createUIComponents();
    }

    private static void useSystemTray() {

        InputStream trayIconStream = SignerApp.class.getResourceAsStream("/org/universis/signer/public/universis-signer-icon.png");
        systemTray = SystemTray.get();
        if (systemTray == null) {
            log.error("Unable to load SystemTray. Signer controller will be loaded as standalone application.");
            // create controller
            currentController = new SignerController();
            currentController.standalone = true;
            // set visibility
            currentController.pack();
            currentController.setVisible(true);
            return;
        }
        try {
            systemTray.setImage(trayIconStream);
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
        if (signerApp != null) {
            systemTray.setStatus("Universis signer is running");
        } else {
            systemTray.setStatus("Universis signer is stopped");
        }
        systemTray.getMenu().add(new MenuItem("About", (ActionListener) e -> {
            try {
                // if current controller exists
                if (currentController != null) {
                    // set visibility
                    currentController.setVisible(true);
                    // bring to front
                    currentController.toFront();
                    // and repaint
                    currentController.repaint();
                    return;
                }
                // otherwise create controller
                currentController = new SignerController();
                currentController.pack();
                currentController.setVisible(true);
            } catch (Exception err) {
                log.error(err);
                log.error(ExceptionUtils.getStackTrace(err));
                JOptionPane.showMessageDialog(null,
                        "An error occurred while showing service controller. " + err.getMessage(),
                        "Universis signer",
                        JOptionPane.ERROR_MESSAGE);
            }

        }));
        systemTray.getMenu().add(new MenuItem("Hide", (ActionListener) e -> {
            if (currentController == null) {
                // do nothing
                return;
            }
            // hide controller
            currentController.setVisible(false);
        }));
        systemTray.getMenu().add(new MenuItem("Quit", (ActionListener) e -> {
            stopServiceAndExit();
        }));
    }

    private static void stopServiceAndExit() {
        try {
            int confirmClose = JOptionPane.showConfirmDialog(null,
                    "You are going to stop Universis signer. Do you want to continue?", "Universis Signer", JOptionPane.OK_CANCEL_OPTION);
            if (confirmClose == 2) {
                return;
            }
            // stop service
            stopService();
            // dispose current controller
            if (currentController != null) {
                currentController.dispose();
                currentController = null;
            }
            // shutdown system tray
            if (systemTray != null) {
                systemTray.shutdown();
            }
            // finally exit
            System.exit(0);
        } catch (Exception err) {
            log.error(err);
            log.error(ExceptionUtils.getStackTrace(err));
            JOptionPane.showMessageDialog(null,
                    "An error occurred while stopping service. " + err.getMessage(),
                    "Universis signer",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Starts universis signer service asynchronously
     *
     * @param configuration {SignerAppConfiguration}
     */
    public static void startService(SignerAppConfiguration configuration) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    TimeUnit.SECONDS.sleep(3);
                    signerApp = new SignerApp(configuration);
                    signerApp.start();
                    // set system tray status label
                    if (systemTray != null) {
                        systemTray.setStatus("Universis signer is running");
                    }
                } catch (IOException | InterruptedException e) {
                    log.error(ExceptionUtils.getStackTrace(e));
                }
            }
        }).start();

    }

    /**
     * Stops universis signer service
     */
    public static void stopService() {
        if (signerApp != null) {
            signerApp.stop();
            if (systemTray != null) {
                systemTray.setStatus("Universis signer is stopped");
            }
        }
    }

    private void onStartStop() {
        // add your code here
        dispose();
    }

    private void onClose() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    stopServiceAndExit();
                } catch (Exception e) {
                    log.error(ExceptionUtils.getStackTrace(e));
                }
            }
        }).start();
    }

    public static void main(String[] args) {

        try {
            log.info("Starting Universis signer controller");
            SignerAppConfiguration configuration = new SignerAppConfiguration();
            // get command-line arguments
            log.debug("Getting command-line arguments");
            Options options = new Options();
            Option configOption = new Option("c", "config", true, "service configuration e.g. -config ./service.properties");
            configOption.setRequired(false);
            options.addOption(configOption);
            // parse arguments
            CommandLineParser parser = new DefaultParser();
            HelpFormatter formatter = new HelpFormatter();
            CommandLine cmd;
            try {
                cmd = parser.parse(options, args);
            } catch (ParseException e) {
                log.error(e);
                log.error(ExceptionUtils.getStackTrace(e));
                System.exit(1);
                return;
            }
            String configurationFile = cmd.getOptionValue("config");
            // load configuration
            Properties properties = new Properties();
            InputStream inputStream;
            if (configurationFile != null) {
                log.info("Try to load configuration from file");
                inputStream = new FileInputStream(configurationFile);
            } else {
                // try to load configuration from current directory
                if (new File("extras/service.properties").exists()) {
                    log.info("Try to load configuration from default location");
                    inputStream = new FileInputStream("extras/service.properties");
                } else {
                    log.info("Try to load configuration from resource");
                    // load from resource stream
                    inputStream = SignerController.class.getResourceAsStream("service.properties");
                }
            }
            properties.load(inputStream);
            configuration.keyStore = properties.getProperty("keyStore");
            configuration.storeType = properties.getProperty("storeType");
            configuration.providerArg = properties.getProperty("providerArg");
            configuration.daemon = true;
            // load extra routes
            configuration.loadRoutesFromFile("extras/routes.xml");
            // start universis signer
            log.info("Starting Universis signer service");
            SignerController.startService(configuration);
            // register system tray
            log.info("Registering application to system tray");
            SignerController.useSystemTray();
        } catch (Exception err) {
            log.error("Universis signer service controller failed to start.");
            log.error(ExceptionUtils.getStackTrace(err));
            System.exit(1);
        }


    }

    private void createUIComponents() {
        setContentPane(contentPane);
        this.setResizable(false);
        // setModal(true);
        setTitle("Universis signer controller");

        // prepare ok button
        buttonStartStop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onStartStop();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onClose();
            }
        });

        // set last message
        if (signerApp != null) {
            if (signerApp.wasStarted()) {
                lastMessage.setText("Universis signer has been successfully started.");
                lastMessage.setForeground(Color.decode("#0000ff"));
            } else {
                lastMessage.setText("Universis signer has not been started yet.");
                lastMessage.setForeground(Color.decode("#0000ff"));
            }
        } else {
            lastMessage.setText("Universis signer has not been started yet.");
            lastMessage.setForeground(Color.decode("#ff00ff"));
        }


        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                // hide controller  (do not close)
                if (standalone) {
                    stopServiceAndExit();
                    return;
                }
                setVisible(false);
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (standalone) {
                    stopServiceAndExit();
                    return;
                }
                // hide controller (do not close)
                setVisible(false);
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        contentPane = new JPanel();
        contentPane.setLayout(new GridLayoutManager(3, 1, new Insets(10, 10, 10, 10), -1, -1));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel1, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel1.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, true, false));
        panel1.add(panel2, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        buttonStartStop = new JButton();
        buttonStartStop.setEnabled(false);
        buttonStartStop.setText("Stop service");
        panel2.add(buttonStartStop, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        buttonCancel = new JButton();
        buttonCancel.setText("Stop service and exit");
        panel2.add(buttonCancel, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(24, 300), null, 0, false));
        splash = new JLabel();
        splash.setBackground(new Color(-1));
        splash.setIcon(new ImageIcon(getClass().getResource("/org/universis/signer/public/universis-signer-label.png")));
        splash.setText("");
        panel3.add(splash, new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lastMessage = new JLabel();
        lastMessage.setText("Universis Signer is starting...");
        lastMessage.setVerticalAlignment(1);
        lastMessage.setVerticalTextPosition(0);
        panel3.add(lastMessage, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }
}
