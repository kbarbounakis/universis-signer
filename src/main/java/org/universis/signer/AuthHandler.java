package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.auth.UsernamePasswordCredentials;

import java.util.Map;

public class AuthHandler  implements RouterNanoHTTPD.UriResponder  {
    @Override
    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return this.other(NanoHTTPD.Method.GET.name(), uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return this.other(NanoHTTPD.Method.PUT.name(), uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return this.other(NanoHTTPD.Method.POST.name(), uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return this.other(NanoHTTPD.Method.DELETE.name(), uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        // validate user
        UsernamePasswordCredentials user = AuthHandler.getUser(ihttpSession);
        if (user == null) {
            return new ForbiddenHandler().get(uriResource, map, ihttpSession);
        }
        return null;
    }

    static UsernamePasswordCredentials getUser(NanoHTTPD.IHTTPSession ihttpSession) {
        // get authorization header
        String[] usernamePassword;
        String authorizationHeader = ihttpSession.getHeaders().get("authorization");
        if (authorizationHeader != null && authorizationHeader.startsWith("Basic ")) {
            byte[] decodedBytes = Base64.decodeBase64(authorizationHeader.replaceFirst("Basic ", ""));
            usernamePassword = new String(decodedBytes).split(":");
            return new UsernamePasswordCredentials(usernamePassword[0], usernamePassword[1]);
        }
        return null;
    }

}
