package org.universis.signer;

import java.util.ArrayList;
import java.util.List;

public class SignatureBlockConfiguration {
    public List<String> signatureBlockText = new ArrayList<>();
    public String signatureBlockTextPosition = SignatureBlockTextPosition.LeftTop;
    public int signatureBlockWidth = 300;
    public int signatureBlockHeight = 80;
}
