package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class FileStreamHandler extends RouterNanoHTTPD.DefaultStreamHandler {

    private static final Logger log = LogManager.getLogger(FileStreamHandler.class);
    private final String file;
    private final String mimeType;

    public FileStreamHandler(String file, String mimeType) {
        this.file = file;
        this.mimeType = mimeType;
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public NanoHTTPD.Response.IStatus getStatus() {
        return NanoHTTPD.Response.Status.OK;
    }

    public InputStream getData() {
        try {
            return new FileInputStream(this.file);
        } catch (FileNotFoundException e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
        return null;
    }
}
