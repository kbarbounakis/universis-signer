package org.universis.signer;


import java.util.Date;

public class SignatureProperties {
    public Date signingDate;
    public String reason;
    public X509CertificateInfo signingCertificate;
}
