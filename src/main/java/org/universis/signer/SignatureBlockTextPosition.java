package org.universis.signer;

public class SignatureBlockTextPosition {
    public final static String LeftTop = "left top";
    public final static String CenterTop = "center top";
    public final static String RightTop = "right top";
    public final static String LeftBottom = "left bottom";
    public final static String CenterBottom = "center bottom";
    public final static String RightBottom = "right bottom";

}
