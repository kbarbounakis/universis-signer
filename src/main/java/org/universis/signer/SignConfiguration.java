package org.universis.signer;

import java.awt.*;
import java.util.List;
import java.util.Map;

public class SignConfiguration {
    public String thumbprint;
    public String name;
    public String reason;
    public Rectangle position;
    public String timestampServer;
    public String originalFile;

    static SignConfiguration fromMap(Map<String, List<String>> params) throws InvalidPositionException {
        SignConfiguration res = new SignConfiguration();
        if (params.containsKey("thumbprint")) {
            res.thumbprint = params.get("thumbprint").get(0);
        }
        if (params.containsKey("file")) {
            res.originalFile = params.get("file").get(0);
        }
        if (params.containsKey("name")) {
            res.name = params.get("name").get(0);
        }
        if (params.containsKey("reason")) {
            res.reason = params.get("reason").get(0);
        }
        if (params.containsKey("position")) {
            String requestPosition = params.get("position").get(0);
            String[] dimensions = requestPosition.split(",");
            if (dimensions.length != 4) {
                throw new InvalidPositionException();
            }
            for (int i = 0; i < dimensions.length; i++) {
                dimensions[i] = dimensions[i].trim();
            }
            res.position = new Rectangle(Integer.parseInt(dimensions[0]), Integer.parseInt(dimensions[1]), Integer.parseInt(dimensions[2]), Integer.parseInt(dimensions[3]));
        }
        if (params.containsKey("timestampServer")) {
            res.timestampServer = params.get("timestampServer").get(0);
        };
        return res;
    }

}
