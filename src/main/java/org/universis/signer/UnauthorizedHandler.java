package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;

/**
 * Handles 403 Forbidden error
 */
public class UnauthorizedHandler extends ErrorHandler {
    public UnauthorizedHandler() {
        super(NanoHTTPD.Response.Status.UNAUTHORIZED);
    }
    public UnauthorizedHandler(Exception e) {
        super(e);
        this.status = NanoHTTPD.Response.Status.UNAUTHORIZED;
    }
}
