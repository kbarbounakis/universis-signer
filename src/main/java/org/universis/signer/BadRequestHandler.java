package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;

/**
 * Handles 400 Bad request error
 */
public class BadRequestHandler extends ErrorHandler {

    public BadRequestHandler() {
        super(NanoHTTPD.Response.Status.BAD_REQUEST);
    }
    public BadRequestHandler(String message) {
        super(NanoHTTPD.Response.Status.BAD_REQUEST, message);
    }
}
