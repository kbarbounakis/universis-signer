package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OfficeDocumentVerifyHandler implements RouterNanoHTTPD.UriResponder {

    private static final Logger log = LogManager.getLogger(SignerHandler.class);
    public HashMap<String,String> files = null;

    public OfficeDocumentVerifyHandler() {
    }

    public OfficeDocumentVerifyHandler(HashMap<String,String> files) {
        this.files = files;
    }

    @Override
    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {

        try {
            // parse body
            if (this.files == null) {
                this.files = new HashMap<>();
                ihttpSession.parseBody(this.files);
            }
            // get input file
            String inFile = this.files.get("file");
            if (!this.files.containsKey("file")) {
                return new BadRequestHandler("File parameter is missing").get(uriResource, map, ihttpSession);
            }
            // get open office documents signer
            OfficeDocumentSigner signer = new OfficeDocumentSigner();
            VerifySignatureResult result = signer.verify(new File(inFile));
            // and finally return a JSON array
            List<VerifySignatureResult> results = new ArrayList<VerifySignatureResult>();
            results.add(result);
            NanoHTTPD.Response res = new JsonResponseHandler(results.toArray()).get(uriResource, map, ihttpSession);
            CorsHandler.enable(res);
            return res;

        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
        }
    }

    @Override
    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new CorsHandler().other(s, uriResource, map, ihttpSession);
    }
}
