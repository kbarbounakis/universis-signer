package org.universis.signer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ErrorHandler implements RouterNanoHTTPD.UriResponder {

    private static final Logger log = LogManager.getLogger(ErrorHandler.class);
    protected final String message;
    protected String stackTrace = "";
    protected String type;
    /**
     * Gets or sets the HTTP status of this error
     */
    protected NanoHTTPD.Response.IStatus status = NanoHTTPD.Response.Status.INTERNAL_ERROR;

    public ErrorHandler(String message) {
        // set message (status is 500 by default)
        this.message = message;
    }

    public ErrorHandler(NanoHTTPD.Response.IStatus status) {
        // set message
        this(status.getDescription());
        // set status
        this.status = status;
    }

    public ErrorHandler(NanoHTTPD.Response.IStatus status, String message) {
        // set message
        this(status.getRequestStatus() + " " + message);
        // and status
        this.status = status;
    }

    public ErrorHandler(Exception e) {
        // set message
        this.message = e.getMessage();
        // set type
        this.type = e.getClass().getName();
        // and stack trace
        this.stackTrace = Arrays.toString(e.getStackTrace());
    }

    public String getText() {
        return "<html><body><h3>Error " + this.message +  "</h3>" +
                "<p>" + this.stackTrace + "</p>" +
                "</body></html>";
    }

    public String getJson() {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = new HashMap<>();
        map.put("status", this.getStatus());
        map.put("statusCode", this.getStatus().getRequestStatus());
        map.put("message", this.message);
        if (this.stackTrace != null && this.stackTrace.length() > 0) {
            map.put("stackTrace", this.stackTrace);
        }
        if (this.type != null) {
            map.put("type", this.type);
        }
        try {
            return mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
        return this.getText();
    }

    public String getMimeType() {
        return "text/html";
    }

    public NanoHTTPD.Response.IStatus getStatus() {
        return this.status;
    }

    @Override
    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        // handle application/json request
        String accept = ihttpSession.getHeaders().get("accept");
        NanoHTTPD.Response res;
        if ("application/json".equals(accept)) {
            res = NanoHTTPD.newFixedLengthResponse(this.getStatus(), "application/json", this.getJson());
        } else {
        // handle other requests
            res = NanoHTTPD.newFixedLengthResponse(this.getStatus(), this.getMimeType(), this.getText());
        }
        CorsHandler.enable(ihttpSession, res);
        return res;
    }

    @Override
    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new ErrorHandler(NanoHTTPD.Response.Status.METHOD_NOT_ALLOWED).get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new ErrorHandler(NanoHTTPD.Response.Status.METHOD_NOT_ALLOWED).get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new ErrorHandler(NanoHTTPD.Response.Status.METHOD_NOT_ALLOWED).get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new ErrorHandler(NanoHTTPD.Response.Status.METHOD_NOT_ALLOWED).get(uriResource, map, ihttpSession);
    }
}
