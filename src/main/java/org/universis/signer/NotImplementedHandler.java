package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;

/**
 * Handles 501 Not Implemented
 */
public class NotImplementedHandler extends ErrorHandler {

    public NotImplementedHandler() {
        super(NanoHTTPD.Response.Status.NOT_IMPLEMENTED);
    }
}
