
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.ContentByteUtils;
import com.itextpdf.text.pdf.parser.PdfContentStreamProcessor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.universis.signer.*;
import org.universis.signer.Signer;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.util.List;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class SignerTest {

    @org.junit.jupiter.api.Test
    void sign() throws GeneralSecurityException, IOException, DocumentException {
        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(new FileInputStream(keyStorePath), "secret".toCharArray());
        // create class
        org.universis.signer.Signer signer = new org.universis.signer.Signer(ks);
        // get inFile
        String inFile = SignerTest.class.getResource("loremipsum.pdf").getPath();
        String outFile = new java.io.File( "tmp/signed.pdf" ).getAbsolutePath();
        signer.sign(inFile, outFile, "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238", "secret", null, 1, null, null);
    }

    void signWithToken() throws GeneralSecurityException, IOException, DocumentException {
        // load test keystore
        String configFilepath = SignerTest.class.getResource("eToken.cfg").getPath();
        Provider p = new sun.security.pkcs11.SunPKCS11(configFilepath);
        Security.addProvider(p);
        KeyStore ks = KeyStore.getInstance("pkcs11");

        ks.load(null, "1234".toCharArray());
        // create class
        Signer signer = new org.universis.signer.Signer(ks);
        String inFile = SignerTest.class.getResource("loremipsum.pdf").getPath();
        String outFile = new java.io.File( "tmp/signed.pdf" ).getAbsolutePath();
        signer.sign(inFile, outFile, "F641D66D49BA43BF911E873889D7FA0E32E26CE6", "1234", null, 1, null, null);
    }

    @org.junit.jupiter.api.Test
    void shouldReadSignaturePosition() throws IOException, GeneralSecurityException, DocumentException {
        String inFile = SignerTest.class.getResource("report.pdf").getPath();
        PdfReader reader = new PdfReader(inFile);
        SignaturePositionListener listener = new SignaturePositionListener();
        listener.configuration.signatureBlockTextPosition = SignatureBlockTextPosition.LeftTop;
        listener.configuration.signatureBlockText.add("Digital signature");
        PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
        int numberOfPages = reader.getNumberOfPages();
        for (int pageNum = 1; pageNum <= numberOfPages; pageNum++) {
            PdfDictionary pageDic = reader.getPageN(pageNum);
            PdfDictionary resourcesDic = pageDic.getAsDict(PdfName.RESOURCES);
            processor.processContent(ContentByteUtils.getContentBytesForPage(reader, pageNum), resourcesDic);
        }
        assertNotNull(listener.position);

        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(new FileInputStream(keyStorePath), "secret".toCharArray());

        Signer signer = new Signer(ks);
        String outFile = new java.io.File( "tmp/report_signed.pdf").getAbsolutePath();
        Rectangle rect = listener.position;
        signer.sign(inFile, outFile, "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238", "secret", "Test Signer", 1, rect, null);

    }

    @org.junit.jupiter.api.Test
    void shouldReadCustomSignaturePosition() throws IOException, GeneralSecurityException, DocumentException {
        String inFile = SignerTest.class.getResource("reportSigCenterTop.pdf").getPath();
        PdfReader reader = new PdfReader(inFile);
        SignaturePositionListener listener = new SignaturePositionListener();
        listener.configuration.signatureBlockText.add("Place of digital signature");
        listener.configuration.signatureBlockTextPosition = SignatureBlockTextPosition.CenterTop;
        PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
        int numberOfPages = reader.getNumberOfPages();
        for (int pageNum = 1; pageNum <= numberOfPages; pageNum++) {
            PdfDictionary pageDic = reader.getPageN(pageNum);
            PdfDictionary resourcesDic = pageDic.getAsDict(PdfName.RESOURCES);
            processor.processContent(ContentByteUtils.getContentBytesForPage(reader, pageNum), resourcesDic);
        }
        assertNotNull(listener.position);

        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(new FileInputStream(keyStorePath), "secret".toCharArray());

        Signer signer = new Signer(ks);
        String outFile = new java.io.File( "tmp/reportSigCenterTop_signed.pdf").getAbsolutePath();
        Rectangle rect = listener.position;
        signer.sign(inFile, outFile, "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238", "secret", "Test Signer", 1, rect, null);

    }

    @org.junit.jupiter.api.Test
    void shouldReadCenterTopSignaturePosition() throws IOException, GeneralSecurityException, DocumentException {
        String inFile = SignerTest.class.getResource("Blank_A4_CenterTop.pdf").getPath();
        PdfReader reader = new PdfReader(inFile);
        SignaturePositionListener listener = new SignaturePositionListener();
        listener.configuration.signatureBlockText.add("(Signature)");
        listener.configuration.signatureBlockTextPosition = SignatureBlockTextPosition.CenterTop;
        listener.configuration.signatureBlockWidth = 290;
        listener.configuration.signatureBlockHeight = 70;
        PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
        int numberOfPages = reader.getNumberOfPages();
        for (int pageNum = 1; pageNum <= numberOfPages; pageNum++) {
            PdfDictionary pageDic = reader.getPageN(pageNum);
            PdfDictionary resourcesDic = pageDic.getAsDict(PdfName.RESOURCES);
            processor.processContent(ContentByteUtils.getContentBytesForPage(reader, pageNum), resourcesDic);
        }
        assertNotNull(listener.position);

        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(new FileInputStream(keyStorePath), "secret".toCharArray());

        Signer signer = new Signer(ks);
        String outFile = new java.io.File( "tmp/Blank_A4_CenterTop_signed.pdf").getAbsolutePath();
        Rectangle rect = listener.position;
        String inImage = SignerTest.class.getResource("TestSigner.png" ).getPath();
        signer.sign(inFile, outFile, "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238", "secret", "Test Signer", 1, rect, null, null, inImage);

    }

    @org.junit.jupiter.api.Test
    void signWithImage() throws GeneralSecurityException, IOException, DocumentException {
        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(new FileInputStream(keyStorePath), "secret".toCharArray());
        // create class
        org.universis.signer.Signer signer = new org.universis.signer.Signer(ks);
        // get inFile
        String inFile = SignerTest.class.getResource("Blank_A4_LeftTop.pdf").getPath();
        String outFile = new java.io.File( "tmp/Blank_A4_LeftTop_signed.pdf" ).getAbsolutePath();
        String inImage = SignerTest.class.getResource("TestSigner.png" ).getPath();

        signer.sign(inFile, outFile, "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238", "secret", null, 1, null, null, null, inImage);
    }

    @org.junit.jupiter.api.Test
    void shouldReadLeftTopSignaturePosition() throws IOException, GeneralSecurityException, DocumentException {
        String inFile = SignerTest.class.getResource("Blank_A4_LeftTop.pdf").getPath();
        PdfReader reader = new PdfReader(inFile);
        SignaturePositionListener listener = new SignaturePositionListener();
        listener.configuration.signatureBlockText.add("(Signature)");
        listener.configuration.signatureBlockTextPosition = SignatureBlockTextPosition.LeftTop;
        listener.configuration.signatureBlockWidth = 290;
        listener.configuration.signatureBlockHeight = 70;
        PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
        int numberOfPages = reader.getNumberOfPages();
        for (int pageNum = 1; pageNum <= numberOfPages; pageNum++) {
            PdfDictionary pageDic = reader.getPageN(pageNum);
            PdfDictionary resourcesDic = pageDic.getAsDict(PdfName.RESOURCES);
            processor.processContent(ContentByteUtils.getContentBytesForPage(reader, pageNum), resourcesDic);
        }

        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(new FileInputStream(keyStorePath), "secret".toCharArray());

        Signer signer = new Signer(ks);
        String outFile = new java.io.File( "tmp/Blank_A4_LeftTop_signed.pdf").getAbsolutePath();
        Rectangle rect = listener.position;
        String inImage = SignerTest.class.getResource("TestSigner.png" ).getPath();
        signer.sign(inFile, outFile, "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238", "secret", "Test Signer", 1, rect, null, null, inImage);

    }

    @org.junit.jupiter.api.Test
    void shouldReadRightTopSignaturePosition() throws IOException, GeneralSecurityException, DocumentException {
        String inFile = SignerTest.class.getResource("Blank_A4_RightTop.pdf").getPath();
        PdfReader reader = new PdfReader(inFile);
        SignaturePositionListener listener = new SignaturePositionListener();
        listener.configuration.signatureBlockText.add("(Signature)");
        listener.configuration.signatureBlockTextPosition = SignatureBlockTextPosition.RightTop;
        listener.configuration.signatureBlockWidth = 290;
        listener.configuration.signatureBlockHeight = 70;
        PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
        int numberOfPages = reader.getNumberOfPages();
        for (int pageNum = 1; pageNum <= numberOfPages; pageNum++) {
            PdfDictionary pageDic = reader.getPageN(pageNum);
            PdfDictionary resourcesDic = pageDic.getAsDict(PdfName.RESOURCES);
            processor.processContent(ContentByteUtils.getContentBytesForPage(reader, pageNum), resourcesDic);
        }

        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(new FileInputStream(keyStorePath), "secret".toCharArray());

        Signer signer = new Signer(ks);
        String outFile = new java.io.File( "tmp/Blank_A4_RightTop_signed.pdf").getAbsolutePath();
        Rectangle rect = listener.position;
        String inImage = SignerTest.class.getResource("TestSigner.png" ).getPath();
        signer.sign(inFile, outFile, "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238", "secret", "Test Signer", 1, rect, null, null, inImage);

    }

    @org.junit.jupiter.api.Test
    void shouldReadCenterBottomSignaturePosition() throws IOException, GeneralSecurityException, DocumentException {
        String inFile = SignerTest.class.getResource("Blank_A4_CenterBottom.pdf").getPath();
        PdfReader reader = new PdfReader(inFile);
        SignaturePositionListener listener = new SignaturePositionListener();
        listener.configuration.signatureBlockText.add("(Signature)");
        listener.configuration.signatureBlockTextPosition = SignatureBlockTextPosition.CenterBottom;
        listener.configuration.signatureBlockWidth = 290;
        listener.configuration.signatureBlockHeight = 70;
        PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
        int numberOfPages = reader.getNumberOfPages();
        for (int pageNum = 1; pageNum <= numberOfPages; pageNum++) {
            PdfDictionary pageDic = reader.getPageN(pageNum);
            PdfDictionary resourcesDic = pageDic.getAsDict(PdfName.RESOURCES);
            processor.processContent(ContentByteUtils.getContentBytesForPage(reader, pageNum), resourcesDic);
        }
        assertNotNull(listener.position);

        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(new FileInputStream(keyStorePath), "secret".toCharArray());

        Signer signer = new Signer(ks);
        String outFile = new java.io.File( "tmp/Blank_A4_CenterBottom_signed.pdf").getAbsolutePath();
        Rectangle rect = listener.position;
        String inImage = SignerTest.class.getResource("TestSigner.png" ).getPath();
        signer.sign(inFile, outFile, "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238", "secret", "Test Signer", 1, rect, null, null, inImage);

    }

    @org.junit.jupiter.api.Test
    void shouldReadLeftBottomSignaturePosition() throws IOException, GeneralSecurityException, DocumentException {
        String inFile = SignerTest.class.getResource("Blank_A4_LeftBottom.pdf").getPath();
        PdfReader reader = new PdfReader(inFile);
        SignaturePositionListener listener = new SignaturePositionListener();
        listener.configuration.signatureBlockText.add("(Signature)");
        listener.configuration.signatureBlockTextPosition = SignatureBlockTextPosition.LeftBottom;
        listener.configuration.signatureBlockWidth = 290;
        listener.configuration.signatureBlockHeight = 70;
        PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
        int numberOfPages = reader.getNumberOfPages();
        for (int pageNum = 1; pageNum <= numberOfPages; pageNum++) {
            PdfDictionary pageDic = reader.getPageN(pageNum);
            PdfDictionary resourcesDic = pageDic.getAsDict(PdfName.RESOURCES);
            processor.processContent(ContentByteUtils.getContentBytesForPage(reader, pageNum), resourcesDic);
        }
        assertNotNull(listener.position);

        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(new FileInputStream(keyStorePath), "secret".toCharArray());

        Signer signer = new Signer(ks);
        String outFile = new java.io.File( "tmp/Blank_A4_LeftBottom_signed.pdf").getAbsolutePath();
        Rectangle rect = listener.position;
        String inImage = SignerTest.class.getResource("TestSigner.png" ).getPath();
        signer.sign(inFile, outFile, "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238", "secret", "Test Signer", 1, rect, null, null, inImage);

    }

    @org.junit.jupiter.api.Test
    public void shouldVerifySignature() throws IOException, InvalidFormatException, GeneralSecurityException, XMLSignatureException, MarshalException, DocumentException {

        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(new FileInputStream(keyStorePath), "secret".toCharArray());
        // create class
        org.universis.signer.Signer signer = new org.universis.signer.Signer(ks);
        // get inFile
        String inFile = SignerTest.class.getResource("loremipsum.pdf").getPath();
        String outFile = new java.io.File( "tmp/signed.pdf" ).getAbsolutePath();
        signer.sign(inFile, outFile, "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238", "secret", null, 1, null, null);

        signer = new Signer();
        List<VerifySignatureResult> results = signer.verify(new File("tmp/signed.pdf"));
        assertNotNull(results);

    }

    @org.junit.jupiter.api.Test
    void shouldSerializeSignerRoutes() throws JsonProcessingException {
        Properties properties = new Properties();
        SignerRouteConfiguration routeConfiguration = new SignerRouteConfiguration();
        routeConfiguration.routes.add(new SignerRoute() {
            {
                path = "/sign";
                className = "org.universis.SignerHandler";
            }
        });

        routeConfiguration.routes.add(new SignerRoute() {
            {
                path = "/verify";
                className = "org.universis.VerifyHandler";
            }
        });

        XmlMapper xmlMapper = new XmlMapper();
        String xml = xmlMapper.writeValueAsString(routeConfiguration.routes.get(0));
        System.out.println(xml);
        assertNotNull(xml);

        xmlMapper = new XmlMapper();
        xml = xmlMapper.writeValueAsString(routeConfiguration);
        System.out.println(xml);
        assertNotNull(xml);




    }


}