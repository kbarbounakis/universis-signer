:: This script uninstalls universis signer service
@echo off
:: get script path
:: SET script_path=%~dp0
:: get installation directory
for %%i in ("%~dp0..\..") do set "install_dir=%%~fi"
:: stop service
echo Stopping universis signer service
%install_dir%\extras\windows\daemon\amd64\prunsrv.exe //SS//UniversisSigner
:: remove service
echo Removing universis signer service
%install_dir%\extras\windows\daemon\amd64\prunsrv.exe //DS//UniversisSigner
