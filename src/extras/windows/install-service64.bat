:: This script installs universis signer as windows service
@echo off
:: get script path
:: SET script_path=%~dp0
:: get installation directory
for %%i in ("%~dp0..\..") do set "install_dir=%%~fi"
:: install service
echo Installing universis signer as service
%install_dir%\extras\windows\daemon\amd64\prunsrv.exe //IS//UniversisSigner --DisplayName="Universis Signer Service" --Jvm="%install_dir%\jre\bin\client\jvm.dll" --JvmOptions="-Dlog4j.configurationFile=extras/log4j2.properties" --Install=%install_dir%\extras\windows\daemon\amd64\prunsrv.exe --Startup=auto --StartMode=jvm --StopMode=jvm --Classpath=%install_dir%\universis-signer.jar --StartClass=org.universis.signer.SignerService --StartParams=-storeType;PKCS11;-keyStore;none;-providerArg;%install_dir%\extras\windows\eToken.cfg --StopClass=org.universis.signer.SignerService --StdOutput=%install_dir%\log\stdout.log --StdError=%install_dir%\log\stderr.log
:: start service
echo Starting universis signer
%install_dir%\extras\windows\daemon\amd64\prunsrv.exe //ES//UniversisSigner
