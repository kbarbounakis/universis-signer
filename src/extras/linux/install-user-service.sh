#!/bin/sh
# This script installs universis-signer as user service
# Important note: Before running this script move it script to installation root directory

# get script path
SCRIPT=$(readlink -f "$0")

# format script path for sed
SCRIPT_PATH=$(echo $(dirname "$SCRIPT") | sed 's_/_\\/_g')

mkdir -p $HOME/.local/share/systemd/user

STR="s/SERVICE_PATH/$SCRIPT_PATH/g"
# copy service
sed $STR ./extras/linux/universis-signer-user.service > $HOME/.local/share/systemd/user/universis-signer.service

# copy service configuration
cp ./extras/linux/user-service.properties ./extras/service.properties

# change user service permissions
chmod 644 /$HOME/.local/share/systemd/user/universis-signer.service

# enable user service
systemctl --user enable universis-signer.service

# create symlink for eToken lib
[ -f /usr/lib64/libeToken.so ] && ln -s /usr/lib64/libeToken.so /usr/lib/libeToken.so

# start user service
systemctl --user start universis-signer.service
