#define MyAppName "Universis Signer"
#define MyAppVersion "1.8.1"
#define MyAppPublisher "UniverSIS"
#define MyAppURL "https://universis.io"
#define SourceDir "ROOT_DIR"
#define MyAppExeName64 "universis-signer64.exe"
#define MyAppExeName "universis-signer.exe"
#define InstallerBaseName "universis-signer-installer"
#define JAVA_DIR "JAVA_HOME\jre"

[Setup]
AppId={{58A21B47-6717-45FE-9644-1819C37E095C}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName=C:/universis-signer-windows-x64
DisableDirPage=yes
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
DisableWelcomePage=no
OutputBaseFilename={#InstallerBaseName}-{#MyAppVersion}
LicenseFile={#SourceDir}\LICENSE
PrivilegesRequiredOverridesAllowed=dialog
Compression=lzma
SolidCompression=yes
WizardStyle=modern
SetupIconFile={#SourceDir}\src\main\resources\org\universis\signer\public\universis-signer-icon.ico
WizardImageFile={#SourceDir}\src\main\resources\org\universis\signer\public\universis-signer-icon.bmp
UninstallDisplayIcon={#SourceDir}\target\extras\windows\universis-signer64.exe
SetupMutex=UniversisSignerInstallerMutex,Global\UniversisSignerInstallerMutex
AppMutex=UniversisSignerMutex

[Code]
function IsAppRunning(): Boolean;
var
    FSWbemLocator: Variant;
    FWMIService   : Variant;
    FWbemObjectSet: Variant;
begin
    Result := false;
    try
      FSWbemLocator := CreateOleObject('WBEMScripting.SWBEMLocator');
      FWMIService := FSWbemLocator.ConnectServer('', 'root\CIMV2', '', '');
      FWbemObjectSet :=
        FWMIService.ExecQuery('SELECT * FROM Win32_Process Where ExecutablePath="' + 'C:\\universis-signer-windows-x64\\jre\\bin\\javaw.exe' + '"');
      Result := (FWbemObjectSet.Count > 0);
      FWbemObjectSet := Unassigned;
      FWMIService := Unassigned;
      FSWbemLocator := Unassigned;
    except
    Result := false
  end;
end;
function InitializeUninstall(): Boolean;
var
  ErrorCode: Integer;
begin
  if IsAppRunning() then
  begin
    if MsgBox(
         'UniverSIS Signer is currently running in the background. ' +
         'Do you like to close it automatically and uninstall?',
         mbError, MB_YESNO) <> IDYES then
    begin
      Result := False;
      exit;
    end
      else
    begin
      ShellExec(
        'open', 'taskkill.exe', '/f /im javaw.exe', '' , SW_HIDE, ewNoWait, ErrorCode);
    end;
  end;
  Result := True;
end;

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags:

[Files]
Source: "{#SourceDir}\target\extras\windows\eToken.cfg"; DestDir: "{app}\extras\windows"; Flags: ignoreversion onlyifdoesntexist
Source: "{#SourceDir}\target\extras\*"; DestDir: "{app}\extras\"; Excludes: "linux, macosx, eToken.cfg"; Flags: ignoreversion recursesubdirs
Source: "{#SourceDir}\target\lib\*"; DestDir: "{app}\lib\"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#SourceDir}\target\universis-signer.jar"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#SourceDir}\target\universis-signer-harica.jar"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#JAVA_DIR}\*"; DestDir: "{app}\jre\"; Flags: ignoreversion recursesubdirs createallsubdirs

[Dirs]
Name: "{app}\log"

[Icons]
Name: "{autoprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName64}"; Check: IsWin64
Name: "{autoprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Check: not IsWin64
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName64}"; Tasks: desktopicon; Check: IsWin64
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon; Check: not IsWin64

[Run]
Filename: "{app}\extras\windows\install-app64.bat";Description:"Install UniverSIS Signer as a startup application (Reccomended)"; Parameters: "install"; Flags: nowait postinstall runhidden skipifsilent 64bit; Check: IsWin64
Filename: "{app}\extras\windows\install-app.bat";Description:"Install UniverSIS Signer as a startup application (Reccomended)"; Parameters: "install"; Flags: nowait postinstall runhidden skipifsilent 32bit; Check: not IsWin64
Filename: "{app}\extras\windows\install-service64.bat"; Description:"Install UniverSIS Signer as a system service"; Parameters: "install"; Flags: nowait postinstall runhidden skipifsilent unchecked 64bit; Check: IsWin64
Filename: "{app}\extras\windows\install-service.bat"; Description:"Install UniverSIS Signer as a system service"; Parameters: "install"; Flags: nowait postinstall runhidden skipifsilent unchecked 32bit; Check: not IsWin64


[UninstallRun]
Filename: {sys}\sc.exe; Parameters: "stop UniversisSigner";
Filename: {sys}\sc.exe; Parameters: "delete UniversisSigner" ;
Filename: "{app}\extras\windows\uninstall-service64.bat"; Flags: 64bit; Check: IsWin64
Filename: "{app}\extras\windows\uninstall-service.bat"; Flags: 32bit; Check: not IsWin64
Filename: "{app}\extras\windows\uninstall-app64.bat"; Flags: 64bit; Check: IsWin64
Filename: "{app}\extras\windows\uninstall-app.bat"; Flags: 32bit; Check: not IsWin64
